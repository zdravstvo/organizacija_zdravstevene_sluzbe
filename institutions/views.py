from rest_framework import viewsets

from . import models
from . import serializers


class InstitutionTypeViewSet(viewsets.ModelViewSet):
    queryset = models.InstitutionKind.objects.all()
    serializer_class = serializers.InstitutionTypeSerializer


class InstitutionViewSet(viewsets.ModelViewSet):
    queryset = models.Institution.objects.all()
    serializer_class = serializers.InstitutionSerializer


class SubsectionViewSet(viewsets.ModelViewSet):
    queryset = models.Subsection.objects.all()
    serializer_class = serializers.SubsectionSerializer


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = models.Service.objects.all()
    serializer_class = serializers.ServiceSerializer
