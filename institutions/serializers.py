from rest_framework.serializers import ModelSerializer

from . import models


class InstitutionTypeSerializer(ModelSerializer):

    class Meta:
        model = models.InstitutionKind
        fields = '__all__'


class InstitutionSerializer(ModelSerializer):

    class Meta:
        model = models.Institution
        fields = '__all__'


class SubsectionSerializer(ModelSerializer):

    class Meta:
        model = models.Subsection
        fields = '__all__'


class ServiceSerializer(ModelSerializer):

    class Meta:
        model = models.Service
        fields = '__all__'
