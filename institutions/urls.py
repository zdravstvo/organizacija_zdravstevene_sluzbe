from django.urls import include, path

from common.routers import BaseRouter
from . import views


router = BaseRouter()

router.register('institutions', views.InstitutionViewSet)
router.register('services', views.ServiceViewSet)
router.register('types',views.InstitutionTypeViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
