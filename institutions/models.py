from django.db import models

from teritorijalna_organizacija.models import Municipality
from teritorijalna_organizacija.models import Locality
from teritorijalna_organizacija.models import Region


class InstitutionKind(models.Model): #Bolnica,klinika,Apoteka,zavodi,instituti
    name = models.CharField(max_length=20)


class Service(models.Model): # onkologija, pedijatrija, prodaja lekova na malo etc
    name = models.CharField(max_length=20)


class Institution(models.Model): #Medical Institution
    name = models.CharField(max_length=40)
    address = models.CharField(max_length=200)
    telephone = models.CharField(max_length=30)
    email = models.CharField(max_length=40)
    PIB = models.CharField(max_length=20)
    id_number = models.CharField(max_length=20) #maticni broj preduzeca
    fax = models.CharField(max_length=30)
    locality = models.ForeignKey(Locality, on_delete=models.CASCADE)
    services_provided = models.ManyToManyField(
        Service,
        related_name='provider_institutions'
        )
    kind = models.ForeignKey(InstitutionKind, on_delete=models.CASCADE)

class Subsection(models.Model):
    name = models.CharField(max_length=40)
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE)


#Statistika: Koliko institucija daje taj i taj servis u nekom gradu(Stepen razvijenosti onkologije npr)
# Koji sve servisi postoje u banatu
