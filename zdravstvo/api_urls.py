from django.urls import path, include
from rest_framework.documentation import include_docs_urls


urlpatterns = [
    path('docs/', include_docs_urls(title="Zdravstvena organizacija")),
    path('location/', include('teritorijalna_organizacija.urls')),
    path('personnel/', include('personnel.urls')),
]
