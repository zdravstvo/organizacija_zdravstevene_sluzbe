import os

from .settings import * # Extend base settings

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DJANGO_APP_DB_NAME', 'postgres'),
        'USER': os.environ.get('DJANGO_APP_DB_USER', 'postgres'),
        'HOST': os.environ.get('DJANGO_APP_DB_HOST', 'db'),
        'PORT': os.environ.get('DJANGO_APP_DB_PORT', '5432')
    }
}
