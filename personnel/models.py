from django.db import models
from institutions.models import Institution

from teritorijalna_organizacija.validators import is_exact_length


class Person(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    date_of_birth = models.DateField()
    address = models.CharField(max_length=128)
    JMBG = models.CharField(max_length=13, validators=[is_exact_length(13)])
    gender = models.CharField(max_length=1)
    title = models.CharField(max_length=40)


class Education(models.Model):
    EDUCATION_LEVELS = (
        ('PR', 'Primary'),
        ('SE', 'Secondary'),
        ('AC', 'Academic'),
        ('PS', 'Post-secondary'),
    )
    level = models.CharField(choices=EDUCATION_LEVELS, max_length=2)
    provider = models.CharField(max_length=50) # TODO: rename this trash
    started = models.DateField()
    finished = models.DateField(null=True, blank=True)
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name='education_history')


class Position(models.Model):
    name = models.CharField(max_length=20)


class Work(models.Model):
    person = models.ForeignKey(
        Person,
        null=True,
        on_delete=models.SET_NULL,
        related_name='work_history',
        )
    institution = models.ForeignKey(
        Institution,
        null=True,
        on_delete=models.SET_NULL,
        related_name='employee_history',
    )
    position = models.ForeignKey(
        Position,
        null=True,
        on_delete=models.SET_NULL,
        related_name='employees',
    )
    started = models.DateField()
    finished = models.DateField(null=True, blank=True)


class ProfessionalTraining(models.Model):
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name='professional_training',
    )
    name = models.CharField(max_length=60)
    kind = models.CharField(max_length=60)
    started = models.DateField()
    finished = models.DateField(null=True, blank=True)
