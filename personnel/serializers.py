from rest_framework.serializers import ModelSerializer

from . import models

class PersonSerializer(ModelSerializer):

    class Meta:
        model = models.Person
        fields = '__all__'


class EducationSerializer(ModelSerializer):

    class Meta:
        model = models.Education
        fields = '__all__'


class PositionSerializer(ModelSerializer):

    class Meta:
        model = models.Position
        fields = '__all__'


class WorkSerializer(ModelSerializer):

    class Meta:
        model = models.Work
        fields = '__all__'


class ProfessionalTrainingSerializer(ModelSerializer):

    class Meta:
        model = models.ProfessionalTraining
        fields = '__all__'
