from rest_framework.viewsets import ModelViewSet

from . import models
from . import serializers

class PersonViewSet(ModelViewSet):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer


class EducationViewSet(ModelViewSet):
    queryset = models.Education.objects.all()
    serializer_class = serializers.EducationSerializer


class PositionViewSet(ModelViewSet):
    queryset = models.Position.objects.all()
    serializer_class = serializers.PositionSerializer


class WorkViewSet(ModelViewSet):
    queryset = models.Work.objects.all()
    serializer_class = serializers.WorkSerializer

class ProfessionalTrainingViewSet(ModelViewSet):
    queryset = models.ProfessionalTraining.objects.all()
    serializer_class = serializers.ProfessionalTrainingSerializer

