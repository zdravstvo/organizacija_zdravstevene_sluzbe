from django.urls import path, include

from common.routers import BaseRouter
from . import views


router = BaseRouter()

router.register('people', views.PersonViewSet)
router.register('work-history', views.WorkViewSet)
router.register('positions', views.PositionViewSet)
router.register('professional-training', views.ProfessionalTrainingViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
