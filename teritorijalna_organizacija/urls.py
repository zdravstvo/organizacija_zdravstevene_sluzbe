from django.urls import include, path

from common.routers import BaseRouter
from . import views


router = BaseRouter()

router.register('region-types', views.RegionKindViewSet)
router.register('regions', views.RegionViewSet)
router.register('localities', views.LocalityViewSet)
router.register('municipalities', views.MunicipalityViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
