from django.apps import AppConfig


class TeritorijalnaOrganizacijaConfig(AppConfig):
    name = 'teritorijalna_organizacija'
