from rest_framework import viewsets

from . import models
from . import serializers

class RegionKindViewSet(viewsets.ModelViewSet):
    queryset = models.RegionKind.objects.all()
    serializer_class = serializers.RegionKindSerializer


class RegionViewSet(viewsets.ModelViewSet):
    queryset = models.Region.objects.all()
    serializer_class = serializers.RegionSerializer


class LocalityViewSet(viewsets.ModelViewSet):
    queryset = models.Locality.objects.all()
    serializer_class = serializers.LocalitySerializer


class MunicipalityViewSet(viewsets.ModelViewSet):
    queryset = models.Municipality.objects.all()
    serializer_class = serializers.MunicipalitySerializer
