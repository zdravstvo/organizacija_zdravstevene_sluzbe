from django.db import models

from .validators import is_exact_length


class FormOfGovernment(models.Model):
    identifier = models.CharField(
        primary_key=True,
        blank=False,
        max_length=2,
        validators=[is_exact_length(2)],
    )
    name = models.CharField(max_length=40)


class Country(models.Model):
    identifier = models.CharField(
        primary_key=True,
        blank=False,
        max_length=3,
        validators=[is_exact_length(3)],
    )
    name = models.CharField(max_length=40)
    current_government = models.ForeignKey(FormOfGovernment, on_delete=models.CASCADE)


class HistoryOfGovernment(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    FormOfGovernment = models.ForeignKey(FormOfGovernment, on_delete=models.CASCADE)
    started = models.DateField()
    finished = models.DateField(null=True, blank=True)


class RegionKind(models.Model):
    identifier = models.CharField(
        primary_key=True,
        blank=False,
        max_length=2,
        validators=[is_exact_length(2)],
    )
    name = models.CharField(max_length=25)


class Region(models.Model):
    name = models.CharField(max_length=100)
    kind = models.ForeignKey(RegionKind, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)


class Municipality(models.Model):
    name = models.CharField(max_length=100)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)


class Locality(models.Model):
    name = models.CharField(max_length=100)
    PTT = models.CharField(max_length=20)
    municipality = models.ForeignKey(Municipality, on_delete=models.CASCADE)




