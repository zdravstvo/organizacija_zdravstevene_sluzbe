from typing import Callable

from django.core.exceptions import ValidationError


class is_exact_length:
    id = 'is_exact_length'

    def __init__(self, length: int):
        self.length = length

    def __call__(self, string: str):
        if len(string) != self.length:
            raise ValidationError(
                f"Supplied identifier {string} must be of length {self.length}"
            )

    def __eq__(self, other):
        return self.id == other.id


    def deconstruct(self):
        return (
            'teritorijalna_organizacija.validators.is_exact_length',
            [self.length],
            {},
        )
