from rest_framework.serializers import ModelSerializer

from . import models


class RegionKindSerializer(ModelSerializer):

    class Meta:
        model = models.RegionKind
        fields = '__all__'


class RegionSerializer(ModelSerializer):

    class Meta:
        model = models.Region
        fields = '__all__'


class LocalitySerializer(ModelSerializer):

    class Meta:
        model = models.Locality
        fields = '__all__'


class MunicipalitySerializer(ModelSerializer):

    class Meta:
        model = models.Municipality
        fields = '__all__'
