cd ~/apps/organizacija_zdravstevene_sluzbe
git fetch
git reset --hard origin/develop
docker-compose down
docker-compose build backend
docker-compose up -d
exit
